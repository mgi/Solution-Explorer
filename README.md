# MGI Solution Explorer

The MGI Solution Explorer is a tool on the LabVIEW Tools Network to help developers manage complex, multi-project builds in LabVIEW.

Read technical article about the tool on [MGI Website](https://www.mooregoodideas.com/products/solution-explorer/index.html)

## Building Solution Explorer

Solution Explorer is built using Solution Explorer. Building a new version is very easy. Open the file "Solution Explorer.lvsln" with the most recent version of Solution Explorer that you have. It should look like this:

![Solution Explorer Build Example](<./Images/Solution Explorer Build Example.png>)

Then choose whether you want to build a Release or Develop version, update the version number as needed (see next section), and press the Build button. Now you will have your new version of Solution Explorer built.

## Versioning

When you increase the version, make sure you also do the following:
- check in the solution file with the increased version
- add an entry to CHANGELOG.md explaining what was changed
- update the release notes of the package (Solution Explorer --> NIPM Package Properties --> Version Information)
- change the name of the folder that the legal notice is installed in (NIPM Package Properties --> Destinations, eg: "_Legal Information\mgi-solution-explorer 2.5.0" to "_Legal Information\mgi-solution-explorer 2.6.0")

## Distribution and Installation

MGI Solution Explorer is distributed and installed using NI Package manager. A NIPM feed is being maintained so updating can be a breeze. Simply register the appropriate feed in your NIPM installation to receive the updates. [Instructions on registering a feed in NIPM.](http://www.ni.com/documentation/en/ni-package-manager/latest/manual/install-packages-from-feed/)

#### Distribution Links

| Installer                                                                                                    | NIPM Feed                                                |
| ------------------------------------------------------------------------------------------------------------ | -------------------------------------------------------- |
| [Latest Release](https://sln-exp-dist.s3-us-west-1.amazonaws.com/master/MGI+Solution+Explorer+Installer.exe) | https://sln-exp-dist.s3-us-west-1.amazonaws.com/master/  |
| [Latest Beta](https://sln-exp-dist.s3-us-west-1.amazonaws.com/develop/MGI+Solution+Explorer+Installer.exe)   | https://sln-exp-dist.s3-us-west-1.amazonaws.com/develop/ |

### VIPM Dependencies
- MGI 1D Array
- MGI Application Control
- MGI Cluster
- MGI Error Handling
- MGI File
- MGI Monitored Actor
- MGI Numeric
- MGI Panel Manager
- MGI Picture & Image
- MGI Read/Write Anything
- MGI String
- MGI Tree
- OpenG Array Library
- OpenG File Library

### Contribution guidelines

- All code should be written in **LabVIEW 2019**
- Pull requests should be small and concise.

### Questions and Comments

Email [support@mooregoodideas.com](mailto:support@mooregoodideas.com) for comments.
Also consider using this repository's [Issue Tracker](https://gitlab.com/mgi/Solution-Explorer/issues) to submit a bug report or feature request.

## Running Solution Explorer from command line

```bash
# GUI Mode
"MGI Solution Explorer.exe" [<solution>] { [-b] | [-build] | [/b] | [/build] }

# CLI Mode
"MGI Solution Explorer.exe" CLI Build <solution> <configuration> { [-version <version>] |  [-gitversion <version>] } [-rebuild]
```

**`<solution>`**
> Solution file to load. This can be an absolute path, or relative to the current directory.

**`<configuration>`**
> Specifies the build configuration to load (i.e. `Debug` or `Release`).

**`-b`**\
**`-build`**\
**`/b`**\
**`/build`**
> Instructs Solution Explorer to start the build process immediately.

**`-version <version>`**
> When provided, defines the build version number.
>
> Format: `<major>.<minor>.<patch>.<build>`
>
> All version parts must be greater than or equal to zero.
> Use "x" to keep version parts from the solution file.

**`-gitversion <version>`**
> Uses [GitVersion](https://gitversion.net/) to determine the build version number.
>
> Format: `<major>.<minor>.<patch>.<build>`
>
> Each version part must define a [version variable](https://gitversion.net/docs/reference/variables).
> Use "x" to keep version parts from the solution file.

**`-rebuild`**
> Instructs Solution Explorer to rebuild the solution (ignores the build cache).
