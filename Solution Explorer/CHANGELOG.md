### 2.7.0

New supported features:
* Add Cache Folder control for specifying a build cache save location. Leaving this control empty defaults to leaving the build cache file in the same location as the solution file.
* Improved context help for build item configuration UIs

### 2.6.2

New supported features:
* Changed "Set to Global Value" VersionMod back to original behavior of taking the global build number in the installer. (e.g. Global v1.2.3.4 --> Installer v1.2.4)
* Added "Set to Global Value (hold Patch)" as a project VersionMod type for holding the Patch number in installers. (e.g. Global v1.2.3.4 --> Installer v1.2.3) 

Bug fixes:
* Version Mod selector dialog now handles case when panel is closed which was causing application to hang.
* Version Mod dialog and save dialog now launch centered upon the calling VI

### 2.6.1

New supported features:
* Version number is passed into sub-solutions
* Right-clicking on an excluded project item gives the option to include it again
* In a RunVI step, a Version control on the front panel will be initialized with the version
* A template has been added for a RunVI
* In a Shell Exec step, if %Version% is found in the standard input string, it will be replaced by the version number in 1.2.3.4 format

Bug fixes:
* Solution Explorer can't handle relative paths onto network shares e.g. \..\\\mgi1\folder. Now an error is reported when you try to save one.
* Fixed the case of zero conditional disables in a project.

### 2.6.0

New supported features:
* Added options for controlling version numbers other than global application of a single new value.
* Added the ability to modify Conditional Disable Symbols based on configuration.
* Displaying the LabVIEW version in the Configure dialog for a project item.

Bug Fixes:
* Version number wasn't set when a solution was opened via the File menu.
* Excluded items weren't copied when duplicating a configuration.
* List of configurations wouldn't update as configurations were added and deleted.

### 2.5.2

Bug Fix:
* Make sure the "x64" flag is written with consistent capitalization

### 2.5.1

Bug Fix:
* Ensure that the product version of the executables and installers matches the configured version number.

## 2.5.0

New supported features:
* Added support for RT Packages and Exe
* Added support for French, German, Japanese, Korean, and Chinese versions of LabVIEW (previously, some builds would fail in these environments due to differences in the build spec)

The following reported bugs have all been fixed:
* Unable to explore to destination when build spec destination is absolute (and other problems related to relative/absolute paths).
* The version number saved in the lvsln file is not populated in the UI when the lvsoln is loaded from solution explorer vs double clicking a lvsln file on disk
* Sub-Solutions always build as debug when using right-click shortcut to "Rebuild"